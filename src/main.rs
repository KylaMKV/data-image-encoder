use std::env::args;
use std::{fs, process, mem};
use std::path::Path;
fn main() {
    let mut outfl = "out.pgm";
    let helpmsg = "die {{encode, decode}} {{file or string}} [optional arguments]";
    let args: Vec<String> = args().collect();
    let mut index = 0;
    for arg in &args {

        if arg == "-o" || arg == "--output" {
            mem::swap(&mut outfl, &mut args[index + 1].as_str());
        }
        index += 1;
    }
    if args.len() <= 2 {
        println!("{}", helpmsg);
        process::exit(1);
    }
    if args[1] == "encode" {
        if Path::new(&args[2]).exists() {
            println!("Encoding file: {}", args[2]);
            encodefl(fs::read(&args[2]).unwrap(), outfl.to_string());
        } else {
            //println!("Encoding string...");
            encodestr(&args[2], outfl.to_string());
        }
    } else if args[1] == "decode" {
        if Path::new(&args[2]).exists() {
            decode(fs::read(&args[2]).unwrap(), outfl.to_string());
        }
    }
}
fn encodestr(string: &String, outfl: String) {
    println!("Encoding, this may take a while...");
    let mut encoded: String = "".to_string();
    let mut length = string.chars().count();
    let tmp = (length as f32).sqrt();
    let x = tmp.round() as u32;
    let y = tmp.ceil() as u32;
    encoded += format!("P2\n{} {}\n256\n", x, y).as_str();
    for c in string.chars() {
        encoded += &((c as u8).to_string() + "\n")
    }
    while (length as f32).sqrt() != y as f32 {
        length += 1;
        encoded += "256\n";
    }
    if outfl == "stdout" {
        println!("{}", encoded);
    }
    println!("Writing to file: {}", outfl);
    let _ = fs::write(outfl, encoded);
}
fn encodefl(fl: Vec<u8>, outfl: String) {
    println!("Encoding, this may take a while...");
    let mut encoded: String = "".to_string();
    let mut length = fl.len();
    let tmp = (length as f32).sqrt();
    let x = tmp.round() as u32;
    let y = tmp.ceil() as u32;
    encoded += format!("P2\n{} {}\n256\n", x, y).as_str();
    for code in fl {
        encoded += &String::from(format!("{}\n", code));
    }
    while (length as f32).sqrt() != y as f32 {
        length += 1;
        encoded += "256\n";
    }
    println!("Writing to file: {}", outfl);
    let _ = fs::write(outfl, encoded);
}
fn decode(fl: Vec<u8>, outfl: String) {
    println!("Decoding, this may take a while...");
    let mut decoded: Vec<u8> = Vec::new();
    let flcontents = String::from_utf8_lossy(&fl);
    let mut getdata = false;
    for ln in flcontents.to_string().split("\n") {
        if ln == "256" && getdata {
            break;
        } else if getdata {
            let value = match ln.parse::<u8>() {
                Ok(v) => v,
                Err(_) => continue,
            };
            decoded.push(value);
        } else if ln == "256" {
            getdata = true;
        }
    }
    println!("Writing to file: {}", outfl);
    let _ = fs::write(outfl, decoded);
}
