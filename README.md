# Data Image Encoder

this is just a simple program to encode/decode data into an image.



## How-to-use

`die {encode, decode} {file or string} [optional arguments]`

### Optional arguments
```
-o --output     Sets output file(set to 'stdout' to redirect to stdout)
```
#

## Supported image formats

- pgm
### Upcomming Supported image formats
- png
- jpg(?)
- gif(?)

_I'm bad at rust, dont judge my code_
